function fish_mode_prompt --description 'Displays the current mode'
                        # Do nothing if not in vi mode
                        if test "$fish_key_bindings" = "fish_vi_key_bindings"
                            switch $fish_bind_mode
                                case default
                                    echo [N]
                                    __fish_cursor_xterm block
                                case insert
                                    echo [I]
                                    __fish_cursor_xterm line
                                case replace
                                    echo [R]
                                    __fish_cursor_xterm underscore
                                case visual
                                    echo [V]
                            end
                            set_color normal
                            printf " "
                        end
                    end
