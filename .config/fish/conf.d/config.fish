##No Annoyance##
set -U fish_user_paths $HOME/go/bin/ $HOME/bin /opt/depot_tools
set -U fish_greeting 
set -U fish_key_bindings fish_default_key_bindings

#aliases
alias vdf="nvim -d"
alias toRows="awk 'BEGIN { ORS = \" \" } { print }'"
alias vim="nvim"
alias v="nvim"
alias e="emc"
alias m="make"
alias g="git"
alias tb='nc termbin.com 9999'
alias rnotebook='docker run --user root --rm -p 10000:8888 -e JUPYTER_ENABLE_LAB=yes -v "$PWD":/home/jovyan/work -e GRANT_SUDO=yes jupyter/datascience-notebook:latest'
alias ysoserial="/lib/jvm/java-8-openjdk/jre/bin/java -jar /home/atul/tools/ysoserial/target/ysoserial-0.0.6-SNAPSHOT-all.jar"
function muslstable
        docker run -v (pwd):/volume --rm -t clux/muslrust:stable cargo build $argv
end 

function muslnightly
        docker run -v (pwd):/volume --rm -t clux/muslrust:nightly cargo build $argv
end 

setenv EDITOR nvim
setenv GOPATH $HOME/go


##who doesnt like autojump##


if test -f /usr/share/autojump/autojump.fish;
	source /usr/share/autojump/autojump.fish;
end

##completions##

source $HOME/.config/fish/completions/kubectl.fish
source $HOME/.config/fish/completions/minikube.fish

#####function and aliases######
function toclip
        xclip -in -sel clip
end

function filetoclip 
	xclip -sel clip < $argv
end

alias l='exa'
alias ll='exa -l'
alias lll='exa -la'
alias manifest="grep -P '<(activity(-alias)?|service|receiver|provider)(?![^>]*exported=\"false\")' AndroidManifest.xml"
function ssh
	switch $argv[1]
	case "*.amazonaws.com"
		env TERM=xterm-256color /usr/bin/ssh $argv
	case "ubuntu@"
		env TERM=xterm-256color /usr/bin/ssh $argv
	case "*"
		env TERM=xterm-256color /usr/bin/ssh $argv
	end
end

function tobrp
        curl http://sweet.0xatul.me:8080/hell.php\?italian\=pizza\&domain\=$argv |\
        sed -e 's/:80$//g' |\
        sed -e 's/:443$//g' |\
        ruby -e 'STDIN.read.lines.sort!.reverse!.uniq! { |x| x.split(%r{https?://}).last }.each { |x| puts x }' |\
        tee /tmp/toimport &&\
        ffuf -c -u FUZZ -w /tmp/toimport -H "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36" -H "Accept: */*;" -H "Connection: closed" -H "Accept-Language: en" -replay-proxy http://127.0.0.1:8080
end

fish_vi_key_bindings

#prompt
# Fish git prompt
set __fish_git_prompt_showuntrackedfiles 'yes'
set __fish_git_prompt_showdirtystate 'yes'
set __fish_git_prompt_showstashstate ''
set __fish_git_prompt_showupstream 'none'
set -g fish_prompt_pwd_dir_length 3



##colored man output##
# Ref: http://linuxtidbits.wordpress.com/2009/03/23/less-colors-for-man-pages/
setenv LESS_TERMCAP_mb \e'[01;31m'       # begin blinking
setenv LESS_TERMCAP_md \e'[01;38;5;74m'  # begin bold
setenv LESS_TERMCAP_me \e'[0m'           # end mode
setenv LESS_TERMCAP_se \e'[0m'           # end standout-mode
setenv LESS_TERMCAP_so \e'[38;5;246m'    # begin standout-mode - info box
setenv LESS_TERMCAP_ue \e'[0m'           # end underline
setenv LESS_TERMCAP_us \e'[04;38;5;146m' # begin underline

setenv FZF_DEFAULT_COMMAND 'fd --type file --follow'
setenv FZF_CTRL_T_COMMAND 'fd --type file --follow'
setenv FZF_DEFAULT_OPTS '--height 20%'
